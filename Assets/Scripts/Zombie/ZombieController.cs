using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class ZombieController : MonoBehaviour
{
   
    
    // Start is called before the first frame update
    private Rigidbody2D _rigidbody;
    private SpriteRenderer _renderer;
    private Animator _animator;

    private int toco = 0;
    void Start()
    {
        _animator = GetComponent<Animator>();
        _renderer = GetComponent<SpriteRenderer>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        caminaDerecha();
        
        if (toco == 2)
        {
            caminaDerecha();
        }
        
        if (toco == 1)
        {
            voltear();
           
        }

        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "tope")
        {
            Debug.Log("Toco");
            toco = 1;
            
        }
        
        if (other.gameObject.tag == "tope2")
        {
            Debug.Log("Toco2");

            toco = 2;

        }

    }

    private void caminaDerecha()
    {
        _renderer.flipX= false; 
        _rigidbody.velocity = new Vector2(5,_rigidbody.velocity.y);

    }
    
    private void voltear()
    {
        _renderer.flipX= true; 
        _rigidbody.velocity = new Vector2(-5,_rigidbody.velocity.y);
    }
}
